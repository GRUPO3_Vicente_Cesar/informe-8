import socket
import threading

HEADER = 64
PORT = 3074
SERVER = '192.168.1.5'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'


server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind(ADDR)

def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} Autorizando.")
    connected = True
    i = 0
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)

            if i == 0:
                if msg != "02:42:cc:85:e3:c7":
                    conn.send("Conexion fallida".encode(FORMAT))
                    connected = False

                print(f"[NEW CONNECTION] {addr} Conectado.")  
                conn.send("Conexion establecida".encode(FORMAT))

            if msg == DISCONNECT_MESSAGE :
                connected = False
                conn.send("Conexion finalizada".encode(FORMAT))

            if i != 0:
                print(f"[{addr}] {msg}")
                conn.send("Msg received".encode(FORMAT))
            
            
            i += 1

    conn.close()


def start():
    server.listen()
    print(f"[LISTEN] Server is listening on address {ADDR}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")

print("[STARTING] server is running.....")
start()

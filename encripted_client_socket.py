import socket
import re, uuid
from cryptography.fernet import Fernet

HEADER = 64
PORT = 3074
#SERVER = '192.168.0.2'
SERVER = socket.gethostbyname('cesarskatelife.ddns.net')
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
MAC = (':'.join(re.findall('..', '%012x' % uuid.getnode()))) 



def load_key():
    """
    Loads the key named `secret.key` from the current directory.
    """
    return open("secret.key", "rb").read()

key = load_key()





client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(ADDR)

def send(msg):
    message=msg.encode(FORMAT)  #codifica el mensaje
    f = Fernet(key)
    encrypted_message = f.encrypt(message) #Encripta
    msg_length = len(encrypted_message)    #tamaño del mensaje encriptado
    send_length = str(msg_length).encode(FORMAT) 

    send_length += b' '*(HEADER-len(send_length)) 
    client.send(send_length)
    client.send(encrypted_message)

    print(client.recv(2048).decode(FORMAT))

send(MAC)
while True:
    mensaje = input("mensaje a enviar: ")
    send(mensaje)
    send(DISCONNECT_MESSAGE)
    break

